# GTK-pass-hasher
MD5 password hasher with a small GTK GUI

# Building

To compile clone the repo and just run the make file that is included

Make sure you have openssl installed on your OS

On Arch run:

```
$ sudo pacman -S openssl
```
On Ubuntu: 

```
$ sudo apt-get install libssl-dev
```

# Images

![alt text](https://raw.githubusercontent.com/3vasion/GTK-pass-hasher/main/images/NEWUIIMG.png)

![alt text](https://raw.githubusercontent.com/3vasion/GTK-pass-hasher/main/images/few.png)
