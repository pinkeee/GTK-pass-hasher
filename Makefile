# change application name here (executable output name)
TARGET=out
# compiler
CXX=gcc
# debug
DEBUG=-g
# optimisation
OPT=-O0
# warnings
WARN=-Wall

PTHREAD=-pthread

CCFLAGS=$(DEBUG) $(OPT) $(WARN) $(PTHREAD) 

GTKLIB=`pkg-config --cflags --libs gtk+-3.0`

# linker
LD=g++
LDFLAGS=$(PTHREAD) $(GTKLIB) -export-dynamic -lcrypto

OBJS=    main.o hash.o

all: $(OBJS)
	$(LD) -o $(TARGET) $(OBJS) $(LDFLAGS)
    
main.o: src/*.c
	$(CC) -c $(CCFLAGS) src/*.c $(GTKLIB) 
clean:
	rm -f *.o $(TARGET)
