#include <gtk/gtk.h>
#include <openssl/evp.h>
#include <time.h>
#include "main.h"

#define MAX_SIZE 64

static const gchar *password_before;
static const gchar *output;

gchar buf[33];

// still learning how to use structs and pointers well so its staying here for when i have time to update and use it :)
typedef struct {
    GtkWidget *main_dialog; // this is for the Help -> About menu
    GtkWidget *entr_pass;
    GtkWidget *entr_out;
    GtkWidget *lbl_output;

} App_widgets;

GtkWidget *main_dialog;
GtkWidget *entr_pass;
GtkWidget *entr_out;
GtkWidget *lbl_output;

int main(int argc, char *argv[]) {
    GtkBuilder      *builder; 
    GtkWidget       *window;
    App_widgets     *widgets = g_slice_new(App_widgets);

    gtk_init(&argc, &argv);

    builder = gtk_builder_new_from_file("glade/window_main.glade");

    window = GTK_WIDGET(gtk_builder_get_object(builder, "window_main"));

    main_dialog = GTK_WIDGET(gtk_builder_get_object(builder, "main_dialog"));
    entr_pass = GTK_WIDGET(gtk_builder_get_object(builder, "entr_pass"));
    entr_out = GTK_WIDGET(gtk_builder_get_object(builder, "entr_out"));
    lbl_output = GTK_WIDGET(gtk_builder_get_object(builder, "lbl_output"));
    
    //widgets -> entr_pass = GTK_WIDGET(gtk_builder_get_object(builder, "entr_pass"));
    //widgets -> entr_out = GTK_WIDGET(gtk_builder_get_object(builder, "entr_out"));

    gtk_builder_connect_signals(builder, widgets);

    g_object_unref(builder);
    gtk_widget_show_all(window);

    gtk_main();

    g_slice_free(App_widgets, widgets);

    return 0;
}

void write_d(gchar *buf, const gchar *data, gint len) {
    password_before = gtk_entry_get_text(GTK_ENTRY(entr_pass));
    gchar *pass_d = "\n\nPassword:  ";

    gchar before[64], pass_combine[64];

    snprintf(before, strlen(pass_d), "%s", pass_d);
    snprintf(pass_combine, strlen(password_before), "%s", password_before);
    
    time_t rawtime;
    struct tm * timeinfo;

    time (&rawtime);
    timeinfo = localtime (&rawtime);
    
    //g_print("[*] Time: %s", asctime(timeinfo));
    gchar *hash = malloc(32);
    gchar buffer[64], h_buff[32], t_buff[64];
    
    gchar *n = "============ TOP OF FILE ============\n\n[*]  ";
    gchar *h = "\nHash:  ";
    gchar *t = asctime(timeinfo);

    snprintf(buffer, strlen(n), "%s", n);
    snprintf(h_buff, strlen(h), "%s", h);
    snprintf(t_buff, strlen(t), "%s", t);
    
    FILE *fPtr; // Creates file pointer for text file writting
    fPtr = (FILE*) malloc(1*sizeof(MAX_SIZE));

    fPtr = fopen("OUTPUT.txt", "w");

    if (fPtr == NULL) {
        g_print("[*] Error, unable to create file\n");
        exit(EXIT_FAILURE);
    }

    fputs(buffer, fPtr);
    fputs(t_buff, fPtr);
    fputs(before, fPtr);
    fputs(pass_combine, fPtr);
    fputs(h_buff, fPtr);

    hash = MD5(buf, data, strlen(data));
    fputs(hash, fPtr);

    g_print("[*] Write to file\n");

    fclose(fPtr);
}

// for the Help -> About menu
void on_btn_help_activate(void) {
    gtk_dialog_run(GTK_DIALOG(main_dialog));
    gtk_widget_hide(main_dialog);
}

void on_window_main_destroy(void) {
    gtk_main_quit();
}

gchar on_btn_hash_clicked(App_widgets *widgets) {
    password_before = gtk_entry_get_text(GTK_ENTRY(entr_pass));
    gchar *hash;

    if (!g_strcmp0(password_before, "")) {
        gchar empty_error[] = "<span foreground='red' weight='bold' font='8'>ENTER A PASSWORD!</span>";
        gtk_label_set_markup(GTK_LABEL(lbl_output), empty_error);
        gtk_entry_set_text(GTK_ENTRY(entr_out), "");

    } else {
        gchar *reset = "";
        gtk_label_set_text(GTK_LABEL(lbl_output), reset);

        const gchar *text = password_before;
        //gchar buf[33];
        
        hash = MD5(buf, text, strlen(text));

        gtk_entry_set_text(GTK_ENTRY(entr_out), hash);

        g_print("[*] HASH: %s\n", buf); // we use %s because 'buf' is a gchar (char) so we do not need to use something like %02x for formatting

        write_d(buf, text, strlen(text));
    }
    return *hash;
}


void on_btn_hash_check_clicked() {
    password_before = gtk_entry_get_text(GTK_ENTRY(entr_pass));
    output = gtk_entry_get_text(GTK_ENTRY(entr_out));

    gchar *is_empty = "";

    if (!g_strcmp0(password_before, is_empty)) {
        gchar empty_error[] = "<span foreground='red' weight='bold' font='8'>ENTER A PASSWORD!</span>";
        gtk_label_set_markup(GTK_LABEL(lbl_output), empty_error);
        gtk_entry_set_text(GTK_ENTRY(entr_out), is_empty);
    
    } else {
        const gchar *text = password_before;
        //gchar buf[33];
        gchar *hash = MD5(buf, text, strlen(text));
        
        if (!g_strcmp0(hash, output)) {
            gchar valid[] = "<span foreground='green' weight='bold' font='8'>HASH VALID!</span>";
            gtk_label_set_markup(GTK_LABEL(lbl_output), valid);
        } else {
            gchar invalid[] = "<span foreground='red' weight='bold' font='8'>HASH INVALID!</span>";
            gtk_label_set_markup(GTK_LABEL(lbl_output), invalid);
        }    
    }   
}