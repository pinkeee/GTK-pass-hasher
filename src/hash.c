#include <openssl/evp.h>
#include <gtk/gtk.h>

gchar *MD5(gchar *buf, const gchar *data, gint len) {
    EVP_MD_CTX *ctx = EVP_MD_CTX_new();
    const EVP_MD *md = EVP_md5();
    
    unsigned int md_len;
    unsigned char val[EVP_MAX_MD_SIZE];
    
    EVP_DigestInit_ex(ctx, md, NULL);
    EVP_DigestUpdate(ctx, data, len);
    EVP_DigestFinal_ex(ctx, val, &md_len);
    EVP_MD_CTX_free(ctx);
    
    for (unsigned int i=0;i<md_len;i++) {
        snprintf(&(buf[i*2]), 3, "%02x", val[i]);
    }
    return buf;

    free(buf);
} 
